#!/bin/bash

# Function to create a new user
create_user() {
    read -p "Enter the username: " username
    if id "$username" &>/dev/null; then
        echo "User $username already exists."
        return
    fi

    read -p "Should this user be an admin? (y/n): " is_admin
    if [[ "$is_admin" =~ ^[Yy]$ ]]; then
        sudo useradd -m -G wheel -s /bin/bash "$username"
        echo "Created admin user $username with home directory /home/$username"
    else
        sudo useradd -m -s /bin/bash "$username"
        echo "Created regular user $username with home directory /home/$username"
    fi

    # Set password for the new user
    echo "Setting password for $username"
    sudo passwd "$username"
}

# Ask if the user wants to create a new user
read -p "Would you like to create a new user? (y/n): " create_user_choice
if [[ "$create_user_choice" =~ ^[Yy]$ ]]; then
    create_user
fi

# Temporary file to store the downloaded CSV
TEMP_FILE="$HOME/tmp/pkg.csv"

# Check if curl is installed
if ! command -v curl &> /dev/null; then
    echo "Error: curl is required but not installed."
    exit 1
fi

# Check if pacman is installed (should be on Arch)
if ! command -v pacman &> /dev/null; then
    echo "Error: pacman not found. Are you running Arch Linux?"
    exit 1
fi

# Ensure the temporary directory exists
mkdir -p "$HOME/tmp"

# Check if yay is installed; if not, install it
if ! command -v yay &> /dev/null; then
    echo "yay not found. Attempting to install yay to $HOME/.local/src/yay..."

    # Ensure git is installed
    if ! command -v git &> /dev/null; then
        echo "git not found. Installing git..."
        sudo pacman -S --noconfirm git
        if [ $? -ne 0 ]; then
            echo "Failed to install git. AUR packages will be skipped."
        fi
    fi

    # Create directory for yay source
    mkdir -p "$HOME/.local/src"

    # Clone yay repository
    git clone https://aur.archlinux.org/yay.git "$HOME/.local/src/yay"
    if [ $? -ne 0 ]; then
        echo "Failed to clone yay repository. AUR packages will be skipped."
    else
        cd "$HOME/.local/src/yay"

        # Build the package
        makepkg -s --noconfirm
        if [ $? -ne 0 ]; then
            echo "Failed to build yay. AUR packages will be skipped."
        else
            # Find the package file
            PACKAGE_FILE=$(ls yay-*.pkg.tar.zst 2>/dev/null | head -n 1)
            if [ -n "$PACKAGE_FILE" ]; then
                # Install the package
                sudo pacman -U --noconfirm "$PACKAGE_FILE"
                if [ $? -ne 0 ]; then
                    echo "Failed to install yay. AUR packages will be skipped."
                else
                    echo "yay installed successfully."
                fi
            else
                echo "No package file found for yay. AUR packages will be skipped."
            fi
        fi

        # Return to original directory
        cd - > /dev/null
    fi
fi

# Download the CSV file
echo "Downloading package list..."
curl -s -o "$TEMP_FILE" "https://gitlab.com/NicholasasaurusRex/cabs/-/raw/cabs/pkg.csv"

# Check if download was successful
if [ $? -ne 0 ]; then
    echo "Error: Failed to download the CSV file"
    exit 1
fi

# Check if file is empty
if [ ! -s "$TEMP_FILE" ]; then
    echo "Error: Downloaded file is empty"
    rm "$TEMP_FILE"
    exit 1
fi

# Read the file, skipping the first line
echo "Processing packages..."
tail -n +2 "$TEMP_FILE" | while IFS=',' read -r tag package _; do
    # Trim whitespace from tag and package
    tag=$(echo "$tag" | xargs)
    package=$(echo "$package" | xargs)

    # Skip if package name is empty
    if [ -z "$package" ]; then
        echo "Skipping line with no package name"
        continue
    fi

    # Check if the package is already installed
    if pacman -Q "$package" &> /dev/null; then
        echo "Package $package is already installed. Skipping."
        continue
    fi

    # Install based on tag
    if [ -z "$tag" ]; then  # pacman package
        echo "Installing $package with pacman..."
        sudo pacman -S --noconfirm "$package"
    elif [ "$tag" = "A" ]; then  # yay package
        if command -v yay &> /dev/null; then
            echo "Installing $package with yay..."
            yay -S --noconfirm "$package"
        else
            echo "Skipping $package (yay not installed)"
        fi
    else
        echo "Skipping $package (invalid tag: $tag)"
    fi
done

# Clean up
rm "$TEMP_FILE"
echo "Processing complete!"

exit 0
